Gui, 99:Margin, 4, 2
Gui, 99:+LastFound
hSet := WinExist()
Gui, 99:Add, GroupBox, x4 y2 w188 h64, Installation
Gui, 99:Add, Text, x8 y16 w78 h21 +0x200, Setup type:
Gui, 99:Add, Text, x8 y40 w78 h21 +0x200, Settings in:
Gui, 99:Add, DropDownList, x88 y16 w100 h21 R5 AltSubmit vset1 gsetup99, static|portable|custom
Gui, 99:Add, DropDownList, x88 y40 w100 h21 R5 AltSubmit vset2 gupd99, registry|user profile|app. folder

Gui, 99:Add, GroupBox, x194 y2 w128 h64, Emergency
Gui, 99:Add, Button, x198 y16 w120 h24 gresGui99, Reset GUI positions
Gui, 99:Add, Button, x198 y40 w120 h24 gresDef99, Restore defaults

Gui, 99:Add, GroupBox, x4 y68 w318 h78, Appearance
Gui, 99:Add, Text, x8 y96 w40 h16, Icons:
Gui, 99:Add, Text, x51 y85 w38 h38 0x7 Border BackgroundTrans vsetS,
Gui, 99:Add, Text, x51 y85 w38 h38 0x8 BackgroundTrans Hidden vsetC,
Gui, 99:Add, Picture, x54 y88 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhsetI1 vsetI1, 
Gui, 99:Add, Picture, x92 y88 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhsetI2 vsetI2, 
Gui, 99:Add, Picture, x130 y88 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhsetI3 vsetI3, 
Gui, 99:Add, Picture, x168 y88 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhsetI4 vsetI4, 
Gui, 99:Add, Picture, x206 y88 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhsetI5 vsetI5, 
Gui, 99:Add, Picture, x244 y88 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhsetI6 vsetI6, 
Gui, 99:Add, Picture, x282 y88 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhsetI7 vsetI7, 
Gui, 99:Add, Checkbox, x8 y126 w120 h16 vset3 gupd99, Show arrow hint
Gui, 99:Add, Checkbox, x198 y126 w120 h16 vset14 gupd99, No autoexpand

Gui, 99:Add, GroupBox, x4 y148 w208 h48, Network interfaces
Gui, 99:Add, Checkbox, x8 y162 w200 h16 vset4 gupd99, Show all interfaces
Gui, 99:Add, Checkbox, x8 y178 w200 h16 vset5 gupd99, Auto-load last chosen interface

Gui, 99:Add, GroupBox, x214 y148 w108 h48, Start-up
Gui, 99:Add, DropDownList, x220 y168 w100 h20 R5 AltSubmit vset6 gupd99
	, hidden|compact|expanded|last state

Gui, 99:Add, GroupBox, x4 y198 w318 h128, Logging
Gui, 99:Add, Text, x8 y212 w64 h16 +0x200, Log path:
Gui, 99:Add, Edit, x8 y230 w272 h22 -Multi vset7 gupd99, Log path
Gui, 99:Add, Button, x280 y230 w40 h22 vB99setP gsetSelPath99, �
Gui, 99:Add, Checkbox, x8 y254 w200 h16 vset8 gupd99, Log sessions
Gui, 99:Add, Checkbox, x8 y270 w200 h16 vset9 glogLoc99, Log external connections
Gui, 99:Add, DropDownList, x210 y268 w70 h21 R15 E0x1000 vset15 gupd99
	, .1|.25|.5|1|2|4|8|16|32|64|128|256|512
Gui, 99:Add, Text, x281 y268 w38 h21 +0x200 vsetSt1, MB/file
Gui, 99:Add, Checkbox, x8 y286 w200 h16 vset10 gupd99, Log local (LAN) connections
Gui, 99:Add, Text, x8 y302 w200 h20 +0x200 vsetSt2, Maximum traffic chunk each write:
Gui, 99:Add, DropDownList, x210 y302 w70 h21 R10 E0x1000 vset11 gupd99
	, 256|512|1024|2048|4096|8192|16384|32768|65536
Gui, 99:Add, Text, x281 y302 w38 h21 +0x200  vsetSt3, bytes

Gui, 99:Add, GroupBox, x4 y328 w280 h60, Measurement
Gui, 99:Add, Text, x8 y342 w200 h20 +0x200, Measuring rate [seconds]:
Gui, 99:Add, Text, x8 y364 w200 h20 +0x200, Measuring unit type:
Gui, 99:Add, DropDownList, x210 y342 w70 h21 R10 Right vset12 gupd99, .1|.25|.5|1|2|5
Gui, 99:Add, DropDownList, x210 y364 w70 h21 R5 AltSubmit Right vset13 gupd99, decimal|binary

Gui, 99:Add, Button, x8 y392 w60 h30 gsave99, OK
Gui, 99:Add, Button, x72 y392 w60 h30 g99GuiClose, Cancel
Gui, 99:Add, Button, x136 y392 w60 h30 Disabled vB99app gapply99, Apply
; Generated using SmartGuiXP Creator mod 4.3.29.8
Gui, 99:Show, AutoSize Center, %appname% settings
setPop=
gosub populate99
setPop=1
Return
;================================================================
99GuiClose:
;================================================================
; revert all the changes
exit99:
Gui, 99:Destroy
Loop, 7
	DllCall("DeleteObject", Ptr, hBset%A_Index%)
Gui, 1:Default
return
;================================================================
upd99:
;================================================================
if !setPop
	return
Gui, 99:Submit, NoHide
GuiControl, 99:Enable, B99app
return
;================================================================
populate99:
;================================================================
if FileExist(setf := A_AppData "\" appname "\" inifile)
	{
	IniRead, setL, %setf%, Settings, SetupLocation, 2
;	setL=2
	IniRead, set1, %setf%, Settings, SetupType, 3
;	logPath := logPathC
	}
else if FileExist(setf := A_ScriptDir "\" inifile)
	{
	IniRead, setL, %setf%, Settings, SetupLocation, 3
;	setL=3
	IniRead, set1, %setf%, Settings, SetupType, 2
;	logPath := logPathP
	}
else
	{
	RegRead, setL, HKLM, Software\Drugwash\%appname%, SetupLocation
	RegRead, set1, HKLM, Software\Drugwash\%appname%, SetupType
	set1 := set1 ? set1 : 1
;	setL=1
	setL := setL ? setL : 1
	setf=
;	logPath := logPathS
	}
logPath := setL=2 ? logPathC : setL=3 ? logPathP : logPathS
hSetIconset=%hsetI1%,%hsetI2%,%hsetI3%,%hsetI4%,%hsetI5%,%hsetI6%,%hsetI7%
Loop, Parse, hSetIconset, CSV
	hBset%A_Index% := ILC_FitBmp(A_LoopField, hIL2, 15-A_Index, 1)
setIS := color
isLogTraf := logTraf
GuiControl, 99:Choose, set1, %set1%
GuiControl, 99:Choose, set2, %setL%
GuiControl, 99:MoveDraw, setS, % "x" 51+38*(color-1)
GuiControl, 99:MoveDraw, setC, % "x" 51+38*(color-1)
GuiControl, 99:, set3, %showHints%
GuiControl, 99:, set14, %noAE%
GuiControl, 99:, set4, %all%
GuiControl, 99:, set5, %autoLast%
GuiControl, 99:Choose, set6, %fws%
GuiControl, 99:, set7, %logPath%
GuiControl, 99:, set8, %logSess%
GuiControl, 99:, set9, %logTraf%
GuiControl, 99:, set10, %logLTraf%
GuiControl, 99:ChooseString, set15, %maxLLsize%
GuiControl, 99:ChooseString, set11, %maxTSlen%
GuiControl, 99:ChooseString, set12, %mRate%
GuiControl, 99:Choose, set13, % unit=1000 ? 1 : 2
Gui, 99:Submit, NoHide
gosub setup99
gosub logLoc99
return
;================================================================
save99:
;================================================================
setf := setL=3 ? A_ScriptDir "\" inifile : setL=2 ? A_AppData "\" appname "\" inifile : ""
if setL>1		; we have ini file
	{
	gosub save99ini
	RegRead, checkIsReg, HKLM, Software\Drugwash\%appname%, SetupType
	if checkIsReg
		gosub save99reg
	}
else			; write to registry
	{
	gosub save99reg
	IniRead, checkIsIni, %setf%, Settings, SetupType, %A_Space%
	if checkIsIni
		gosub save99ini
	}
if (isLogTraf && !logTraf)	; If traffic logging was previously enabled and now is disabled,
	gosub flushTraf	; flush any unlogged connections to file
goto exit99

save99ini:
; write window positions first!!!
WinGetPos, X1, Y1,,, ahk_id %hMain%
IniWrite, %X1%, %setf%, GuiPos, XPos
IniWrite, %Y1%, %setf%, GuiPos, YPos
WinGetPos, X2, Y2,,, ahk_id %hStat%
IniWrite, %X2%, %setf%, GuiPos, XPos2
IniWrite, %Y2%, %setf%, GuiPos, YPos2

IniWrite, %set1%, %setf%, Settings, SetupType
IniWrite, %set2%, %setf%, Settings, SetupLocation
IniWrite, %setIS%, %setf%, Settings, ColorScheme
IniWrite, %set3%, %setf%, Settings, ShowHints
IniWrite, %set14%, %setf%, Settings, NoAutoexpand
IniWrite, %set4%, %setf%, Settings, ShowAll
IniWrite, %choice%, %setf%, Settings, LastChoice
IniWrite, %set5%, %setf%, Settings, AutoloadLastChoice
IniWrite, % (GetWinVisible(hMain) & 1), %setf%, Settings, MainWinLastState
IniWrite, % (pin ? 1 : 0), %setf%, Settings, MainWinPinned
IniWrite, %set6%, %setf%, Settings, MainWinStartState
IniWrite, %set7%, %setf%, Settings, LogPathC
IniWrite, %set8%, %setf%, Settings, LogSessions
IniWrite, %set9%, %setf%, Settings, LogTraffic
IniWrite, %set10%, %setf%, Settings, LogLANTraffic
IniWrite, %set15%, %setf%, Settings, MaxLANLogSize
IniWrite, %set11%, %setf%, Settings, MaxTrafficLength
IniWrite, %set12%, %setf%, Settings, MeasuringRate
IniWrite, % (set13=1 ? 1000 : 1024), %setf%, Settings, Unit
return

save99reg:
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, XPos, %X1%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, YPos, %Y1%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, XPos2, %X2%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, YPos2, %Y2%

RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, SetupType, %setT%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, SetupLocation, %setL%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ColorScheme, %color%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ShowHints, %showHints%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ShowAll, %all%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LastChoice, %choice%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, AutoloadLastChoice, %autoLast%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MainWinPinned, % (pin ? 1 : 0)
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogSessions, %logSess%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogTraffic, %logTraf%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogLANTraffic, %logLTraf%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MaxTrafficLength, %maxTSlen%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MeasuringRate, %mRate%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, Unit, %unit%
return
;================================================================
apply99:		; Apply button
;================================================================
GuiControl, 99:Disable, B99app
Gui, 99:Submit, NoHide
IfNotExist, %set7%
	FileCreateDir, %set7%
setT := set1
setL := set2
color := setIS
showHints := set3
all := set4
autoLast := set5
logPath := set7
logSess := set8
logTraf := set9
logLTraf := set10
maxTSlen := set11
mRate := set12
unit := set13=1 ? 1000 : 1024
noAE := set14
return
;================================================================
resGui99:		; Reset GUI positions button
;================================================================
WinMove, ahk_id %hMain%, w1x, w1y
IniWrite, %w1x%, %setf%, GuiPos, XPos
IniWrite, %w1y%, %setf%, GuiPos, YPos
WinMove, ahk_id %hStat%, w2x, w2y
IniWrite, %w2x%, %setf%, GuiPos, XPos2
IniWrite, %w2y%, %setf%, GuiPos, YPos2
return
;================================================================
setup99:		; Setup type selection
;================================================================
Gui, 99:Submit, NoHide
if set1=1			; static
	{
	GuiControl, 99:Disable, set2
	GuiControl, 99:Choose, set2, 1
	GuiControl, 99:Disable, set7
	GuiControl, 99:Disable, B99setP
	GuiControl, 99:, set7, %logPathS%
	}
else if set1=2		; portable
	{
	GuiControl, 99:Disable, set2
	GuiControl, 99:Choose, set2, 3
	GuiControl, 99:Disable, set7
	GuiControl, 99:Disable, B99setP
	GuiControl, 99:, set7, %logPathP%
	}
else if set1=3		; custom
	{
	GuiControl, 99:Enable, set2
	GuiControl, 99:Enable, set7
	GuiControl, 99:Enable, B99setP
	GuiControl, 99:Choose, set2, %setL%
	GuiControl, 99:, set7, %logPathC%
	}
Gui, 99:Submit, NoHide
return
;================================================================
logLoc99:
;================================================================
Gui, 99:Submit, NoHide
GuiControl, % "99:" (set9 ? "Enable" : "Disable"), set15
GuiControl, % "99:" (set9 ? "Enable" : "Disable"), setSt1
GuiControl, % "99:" (set9 ? "Enable" : "Disable"), setSt2
GuiControl, % "99:" (set9 ? "Enable" : "Disable"), set10
GuiControl, % "99:" (set9 ? "Enable" : "Disable"), set11
GuiControl, % "99:" (set9 ? "Enable" : "Disable"), setSt3
return
;================================================================
setSelPath99:
;================================================================
FileSelectFolder, i, *%logPathC%, 5, Select path where logs will be saved to:
if (!i OR ErrorLevel)
	return
IfNotExist, %i%
	FileCreateDir, %i%
Sleep, 200
IfNotExist, %i%
	{
	MsgBox, 0x2010, %appname% error, Folder cannot be created.`nPlease select different path/name.
	goto setSelPath99
	}
logPathC := i
GuiControl, 99:, set7, %logPathC%
return
;================================================================
resDef99:
;================================================================
GuiControl, 99:Choose, set1, 2
GuiControl, 99:Choose, set2, 3
GuiControl, 99:MoveDraw, setS, % "x" 51+38*2
GuiControl, 99:MoveDraw, setC, % "x" 51+38*2
GuiControl, 99:, set3, 1
GuiControl, 99:, set14, 0
GuiControl, 99:, set4, 1
GuiControl, 99:, set5, 1
GuiControl, 99:Choose, set6, 2
GuiControl, 99:, set7, %logPathP%
GuiControl, 99:, set8, 1
GuiControl, 99:, set9, 0
GuiControl, 99:, set10, 0
GuiControl, 99:ChooseString, set15, 4
GuiControl, 99:ChooseString, set11, 4096
GuiControl, 99:ChooseString, set12, .5
GuiControl, 99:ChooseString, set13, 1000
GuiControl, 99:Enable, B99app
return
