; requires updates.ahk under AHK Basic, LibraryOp.ahk?
ImageDirectoryEntryToData(base, idx, ByRef sz, map=TRUE)
{
Static load, addr
Global Ptr, PtrP, AStr
if !load
	{
	if !h := _ChkDebugFuncStore(1, "imagehlp.dll")
		{
		msgbox, error in %A_ThisFunc%() -> _ChkDebugFuncStore(imagehlp)
		return
		}
	if !addr := DllCall("GetProcAddress", Ptr, h, AStr, "ImageDirectoryEntryToData", Ptr)
		{
		msgbox, error in %A_ThisFunc%()
		return
		}
	load := TRUE
	}
if !base
	return
map := map ? TRUE : FALSE
return DllCall(addr, Ptr, base, "UInt", map, "UShort", idx, PtrP, sz, Ptr)
}
;####################################################
ImageNtHeader(base)
{
Static load, addr
Global Ptr, AStr
if !load
	{
	if !h := _ChkDebugFuncStore(2, "dbghelp.dll")
		{
		msgbox, error in %A_ThisFunc%() -> _ChkDebugFuncStore(dbghelp)
		return
		}
	if !addr := DllCall("GetProcAddress", Ptr, h, AStr, "ImageNtHeader", Ptr)
		{
		msgbox, error in %A_ThisFunc%()
		return
		}
	load := TRUE
	}
if !base
	return
return DllCall(addr, Ptr, base)
}
;####################################################
ImageRvaToVa(base, nthdr, rva)
{
Static load, addr
Global Ptr, AStr
if !load
	{
	if !h := _ChkDebugFuncStore(2, "dbghelp.dll")
		return
	if !addr := DllCall("GetProcAddress", Ptr, h, AStr, "ImageRvaToVa", Ptr)
		return
	load := TRUE
	}
if !(base OR nthdr OR rva)
	return
return DllCall(addr, Ptr, nthdr, Ptr, base, "UInt", rva, Ptr, 0, Ptr)
}
;####################################################
MulDiv(pStr, type="AStr")
{
Static load, addr
Global Ptr, AW, AStr
if !load
	{
	h := DllCall("GetModuleHandle" AW, "Str", "kernel32", Ptr)
	if !addr := DllCall("GetProcAddress", Ptr, h, AStr, "MulDiv", Ptr)
		return
	load := TRUE
	}
if !pStr
	return
return DllCall(addr, Ptr, pStr, "Int", 1, "Int", 1, %type%)
}
;####################################################
GetProcAddress2(base, strg)
{
Static load, addr
Global Ptr, AW, AStr
if !load
	{
	h := DllCall("GetModuleHandle" AW, "Str", "kernel32", Ptr)
	if !addr := DllCall("GetProcAddress", Ptr, h, AStr, "GetProcAddress", Ptr)
		return
	load := TRUE
	}
if !(base OR strg)
	return
if strg is integer
	return DllCall(addr, Ptr, base, "UInt", strg, Ptr)
else return DllCall(addr, Ptr, base, AStr, strg, Ptr)
}
;####################################################
;####################################################
_ChkDebugFuncStore(idx, name)
{
if !h := _DebugFuncStore(idx)	; use different index for different libraries
	{
	if !h := LoadLibraryEx(name, 0)
		return
	_DebugFuncStore(idx, h)	; use different index for different libraries
	}
return h
}
;####################################################
_DebugFuncStore(idx, handle="")
{
Static
Global Ptr
ofi := A_FormatInteger
SetFormat, Integer, D
idx+=0
SetFormat, Integer, %ofi%
if !idx
	return
if !handle
	return h%idx%
else if handle is integer
	h%idx% := handle
else if (handle = "U" && h%idx%)
	DllCall("FreeLibrary", Ptr, h%idx%)
}
;####################################################
LoadLibraryEx(fname, type=0)
{
; ND=no dependencies DF=data file AP=altered path AN=AP+ND AD=AP+DF
ofi := A_FormatInteger
SetFormat, Integer, D
if type is not integer
	type := type="ND" ? 0x1 : type="DF" ? 0x2: type="AP" ? 0x8 : type="AN" ? 0x9 : Type="AD" ? 0xA : 0x0
type+=0
if type not in 0,1,2,8,9,10
	type=0
SetFormat, Integer, %ofi%
; LOAD_LIBRARY_AS_DATA_FILE shifts handle by 1 byte
SplitPath, fname, j
if !i := DllCall("LoadLibraryEx", "Str", fname, "UInt", NULL, "UInt", type)
	i := DllCall("LoadLibraryEx", "Str", j, "UInt", NULL, "UInt", type)
return i
}

