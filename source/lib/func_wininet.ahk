; � Drugwash 2016 v1.0
; Wrapper for wininet.dll
;#include updates.ahk, call updates()
; #include func_DebugFunctions.ahk
; #include func_FileMap.ahk
; #include func_FormatMessage.ahk
; #include func_GetCSIDL.ahk
; #include func_GetFileVersionInfo.ahk

;================================================================
wininet(pn="", por="")
;================================================================
{
#IncludeAgain __GALS.ahk
}
;================================================================
/*
INTERNET_CONNECTION_MODEM			= 0x1
INTERNET_CONNECTION_LAN			= 0x2
INTERNET_CONNECTION_PROXY			= 0x4
INTERNET_CONNECTION_MODEM_BUSY	=0x8
INTERNET_CONNECTION_OFFLINE		=0x20
INTERNET_CONNECTION_CONFIGURED	=0x40
Availability: 95/98/ME/NT4/2000/XP/Vista/?
Function may also return an undocumented 0x10 among states.
When conversion is enabled (conv=1/TRUE), it is filtered from the output.
Conversion will return a list of human-readable states delimited by `n instead of a numeric value.
*/
;================================================================
InternetGetConnectedState(ByRef st, conv=0)
;================================================================
{
Global PtrP
Static pr, states="Modem,LAN,Proxy,Modem busy,<N/A>,Offline,Configured"
if !pr
	pr:=wininet(A_ThisFunc)		; retrieve procedure address from loader function
r := DllCall(pr, PtrP, st, "UInt", 0)
if !conv
	return r
state := ""
Loop, Parse, states, CSV
	if (st & 2**(A_Index-1))=2**(A_Index-1) && A_Index !=5
		state .= A_LoopField "`n"
StringTrimRight, st, state, 1
return r
}
;================================================================
/*
Availability: 95/98/ME/NT4/2000/XP/Vista/?
*/
;================================================================
InternetCheckConnectionA(host, force=0)
;================================================================
{
Static pr
if !pr
	pr:=wininet(A_ThisFunc)		; retrieve procedure address from loader function
force := force ? TRUE : FALSE
return DllCall(pr, "Str", host, "UInt", force, "UInt", 0)
}
;================================================================
InternetCheckConnectionW(host, force=0)
;================================================================
{
Static pr
if !pr
	pr:=wininet(A_ThisFunc)		; retrieve procedure address from loader function
force := force ? TRUE : FALSE
return DllCall(pr, "Str", host, "UInt", force, "UInt", 0)
}
