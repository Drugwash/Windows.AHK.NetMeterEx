; updated 2018.06.06
;#include updates.ahk
; updates()
;================================================================
GetNumberFormat(nmb, lcid="0", u="")	; (0.x,y / -0.x,y / - 0.x,y / 0.x,y- / / 0.x,y -
;================================================================
{
Global AW, Ptr, A_CharSize, PtrSz
if u
	{
	n := x := y := z := 0
	if InStr(u, "(")=1
		{
		n=0
		StringTrimLeft, u, u, 1
		}
	if InStr(u, "-")=1
		{
		n=1
		if InStr(u, " ")=2
			n=2
		StringTrimLeft, u, u, n
		}
	if InStr(u, "0")=1
		{
		z=1
		StringTrimLeft, u, u, 1
		}
	StringLeft, ths, u, 1
	StringTrimLeft, u, u, 1
	if InStr(u, "32")=1
		{
		x=32
		StringTrimLeft, u, u, 2
		}
	else
		{
		StringLeft, x, u, 1
		StringTrimLeft, u, u, 1
		}
	StringLeft, dec, u, 1
	StringTrimLeft, u, u, 1
	StringLeft, y, u, 1
	StringTrimLeft, u, u, 1
	if y not between 0 and 2
		y=2
	if InStr(u, "-")=1
		n=3
	else if InStr(u, " -")=1
		n=4
	u := TRUE
/*
msgbox,
(
n=%n%
x=%x%
y=%y%
z=%z%
dec=%dec%
ths=%ths%
u=%u%
A_CharSize=%A_CharSize%

nmb=%nmb%
lcid=%lcid%
)
*/
	VarSetCapacity(fmt, 24, 0)	; NUMBERFMT struct
	NumPut(y, fmt, 0, "UInt")	; NumDigits [0-2]
	NumPut(z, fmt, 4, "UInt")	; LeadingZero [0-1]
	NumPut(x, fmt, 8, "UInt")	; Grouping [0-9 / 32]
	NumPut(&dec, fmt, 8+PtrSz, Ptr)	; lpDecimalSep
	NumPut(&ths, fmt, 8+2*PtrSz, Ptr)	; lpThousandSep
	NumPut(n, fmt, 8+3*PtrSz, "UInt")	; NegativeOrder [0-4] (1.1)/-1.1/- 1.1/1.1-/1.1 -
	}
sz := DllCall("GetNumberFormat" AW
	, "UInt", lcid				; LCID
	, "UInt", 0				; 0x80000000 LOCALE_NOUSEROVERRIDE
	, Ptr, &nmb				; lpValue
	, Ptr, (u ? &fmt : "0")		; lpFormat
	, Ptr, &out				; lpNumberStr
	, "UInt", sz := 0)			; cchNumber
if !sz
	return, ErrorLevel := 1
VarSetCapacity(out, sz*A_CharSize, 0)
if !DllCall("GetNumberFormat" AW
	, "UInt", lcid				; LCID
	, "UInt", 0				; 0x80000000 LOCALE_NOUSEROVERRIDE
	, Ptr, &nmb				; lpValue
	, Ptr, (u ? &fmt : "0")		; lpFormat
	, Ptr, &out				; lpNumberStr
	, "UInt", sz)				; cchNumber
	return, ErrorLevel := 1
VarSetCapacity(out, -1)
return out
}
