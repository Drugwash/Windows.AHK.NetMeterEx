; � Drugwash 2016

; #include updates.ahk, call updates()
; #include func_DebugFunctions.ahk
; #include func_FileMap.ahk
; #include func_FormatMessage.ahk
; #include func_GetCSIDL.ahk
; #include func_GetFileVersionInfo.ahk

;================================================================
ws2_32(pn="", por="")
;================================================================
{
#IncludeAgain __GALS.ahk
}
;================================================================
WSAStartup(v="2.0")
;================================================================
{
Global
Static pr, WSADATA, r
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
VarSetCapacity(WSADATA, 394+A_PtrSize, 0)
if r := DllCall(pr, "UShort", SubStr(v, 1, 1) + (SubStr(v, -1) << 8) , Ptr, &WSADATA)
	msgbox, % FormatMessage( A_ThisFunc "()", r)
else return TRUE
}
;================================================================
WSACleanup()
;================================================================
{
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
if r := DllCall(pr)
	msgbox, % FormatMessage( A_ThisFunc "()", WSAGetLastError())
else return TRUE
}
;================================================================
WSAGetLastError()
;================================================================
{
Global
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
return DllCall(pr, "UInt")
}
;================================================================
WSACreateEvent()
;================================================================
{
Global
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
return DllCall(pr, Ptr)
}
;================================================================
WSAResetEvent(h)
;================================================================
{
Global
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
if !DllCall(pr, Ptr, h)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nhEvent=" h, WSAGetLastError())
else return TRUE
}
;================================================================
WSACloseEvent(h)
;================================================================
{
Global
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
if !DllCall(pr, Ptr, h)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nhEvent=" h, WSAGetLastError())
else return TRUE
}
;================================================================
inet_ntoa(n)	; Convert an IP (UInt IN_ADDR structure) to string 123.456.789.012
;================================================================
{
Global Ptr
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
return DllCall(pr, "UInt", n, Ptr)
}
;================================================================
ntohl(n)		; Convert UInt from network byte order to host byte order
;================================================================
{
Global
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
return DllCall(pr, "UInt", n, "UInt")
}
;================================================================
ntohs(n)		; Convert UShort from network byte order to host byte order
;================================================================
{
Global
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
return DllCall(pr, "UShort", n, "UShort")
}
;================================================================
inet_addr(s)	; Convert an IP string to IP in UInt format (IN_ADDR structure)
;================================================================
{
Global Ptr, AStr
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
return DllCall(pr, AStr, s, Ptr)
}
;================================================================
gethostbyaddr(addr, ByRef buf, type=4)	; types: IPv4=4 IPv6=6 NetBIOS=N IPX=X IRDA=I
;================================================================
{
Global Ptr, PtrP, PtrSz
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
t := type=4 ? 2 : type=6 ? 23 : type=N ? 17 : type=X ? 6 : type=I ? 26 : 0
if !n := inet_addr(addr)			; addr is not an IP string
	{
	OutputDebug, % ErrorLevel := FormatMessage(A_ThisFunc "(" addr ") -> inet_addr()`nPointer=" pr, WSAGetLastError())
	return
	}
if r := DllCall(pr, "UIntP", n, "UInt", 4, "UInt", t, Ptr)
	{
	VarSetCapacity(buf, sz := 4*PtrSz, 0)	; hostent structure
	DllCall("msvcrt\memcpy", Ptr, &buf, Ptr, r, "UInt", sz, "CDecl")
	return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`naddr=" addr "`ntype=" t " (param=" type ")", WSAGetLastError())
}
;================================================================
gethostbyname(name, ByRef buf)	; this is not right !!!
;================================================================
{
Global Ptr, PtrSz
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
if r := DllCall(pr, "Str", name, Ptr)
	{
	VarSetCapacity(buf, sz := 4*PtrSz, 0)	; hostent structure
	DllCall("msvcrt\memcpy", Ptr, &buf, Ptr, r, "UInt", sz, "CDecl")
	return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nname=" name, WSAGetLastError())
}
;================================================================
; For the following 3/4 see 'Network awareness in Windows XP' in the docs folder
;================================================================
WSALookupServiceBegin(ByRef qsr, flags)	; Prepare WSAQUERYSET struct externally and pass it as qsr
;================================================================
{
Global Ptr
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
if r := DllCall(pr, Ptr, &qsr, "UInt", flags, PtrP, h)
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nflags=" flags "`nhandle=" h, WSAGetLastError())
else return h
}
;================================================================
WSALookupServiceNext(h, flags, ByRef buf)
;================================================================
{
Global Ptr
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
; to be completed !!!
}
;================================================================
WSALookupServiceEnd(h)
;================================================================
{
Global Ptr
Static pr
if !pr
	pr:=ws2_32(A_ThisFunc)	; retrieve procedure address from loader function
if DllCall(pr, Ptr, h)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nhEvent=" h, WSAGetLastError())
else return TRUE
}
;================================================================
; Add WSANSPIoctl (doesn't exist in the 9x version)
