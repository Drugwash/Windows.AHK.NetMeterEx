; Formatting number to hex string � Drugwash, September 2015-July 2016
; c2s=CharToString, s2s=ShortToString, d2s=DWORDToString, q2s=QWORDToString,StringFromGUID
;================================================================
c2s(n)
{
Global
len=4*A_CharSize
VarSetCapacity(i, len, 0)
DllCall("msvcrt\" (A_IsUnicode ? "swprintf" : "sprintf")
	, Ptr, &i
	, "Str", "0x%02X"
	, "UChar", n
	, "CDecl")
VarSetCapacity(i, -1)
return i
}
;================================================================
s2s(n)
{
Global
len=6*A_CharSize
VarSetCapacity(i, len, 0)
DllCall("msvcrt\" (A_IsUnicode ? "swprintf" : "sprintf")
	, Ptr, &i
	, "Str", "0x%04hX"
	, "UShort", n
	, "CDecl")
VarSetCapacity(i, -1)
return i
}
;================================================================
d2s(n)
{
Global
len=10*A_CharSize
VarSetCapacity(i, len, 0)
DllCall("msvcrt\" (A_IsUnicode ? "swprintf" : "sprintf")
	, Ptr, &i
	, "Str", "0x%08lX"
	, "UInt", n
	, "CDecl")
VarSetCapacity(i, -1)
return i
}
;================================================================
q2s(n)
{
Global
len=18*A_CharSize
VarSetCapacity(i, len, 0)
DllCall("msvcrt\" (A_IsUnicode ? "swprintf" : "sprintf")
	, Ptr, &i
	, "Str", "0x%016llX"
	, "Int64", n
	, "CDecl")
VarSetCapacity(i, -1)
return i
}
;================================================================
StringFromGUID(ByRef guid, off=0)
{
Global
len := 38*A_CharSize
VarSetCapacity(i, len, 0)
DllCall("msvcrt\" (A_IsUnicode ? "swprintf" : "sprintf")
	, Ptr, &i
	, "Str", "{%08lX-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}"
	, "UInt", NumGet(guid, off+0, "UInt")
	, "UShort", NumGet(guid, off+4, "UShort")
	, "UShort", NumGet(guid, off+6, "UShort")
	, "UChar", NumGet(guid, off+8, "UChar")
	, "UChar", NumGet(guid, off+9, "UChar")
	, "UChar", NumGet(guid, off+10, "UChar")
	, "UChar", NumGet(guid, off+11, "UChar")
	, "UChar", NumGet(guid, off+12, "UChar")
	, "UChar", NumGet(guid, off+13, "UChar")
	, "UChar", NumGet(guid, off+14, "UChar")
	, "UChar", NumGet(guid, off+15, "UChar")
	, "CDecl")
VarSetCapacity(i, -1)
return i
}
;================================================================
/*
#include updates.ahk
VarSetCapacity(guid, 16, 0)
NumPut(0x1234567890123456, guid, 0, "UInt64")
NumPut(0x1234567890123456, guid, 8, "UInt64")
msgbox, % c2s(75) "`n" s2s(32694) "`n" d2s(0x053c7080) "`n" q2s(0x465082304634) "`n`n" c2s(0xFF) "`n" s2s(0xFFFF) "`n" d2s(0xFFFFFFFF) "`n" q2s(0xFFFFFFFFFFFFFFFF) "`n" StringFromGUID(guid)
*/
