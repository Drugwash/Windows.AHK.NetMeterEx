; some routines adapted from Matt Pietrek's PEDUMP
;#include updates.ahk
;###############################################
MapFile(filename, idx)
{
Global Ptr, AW
if (hFile := DllCall("CreateFile" AW
			, "Str", filename
			, "UInt", 0x80000000		; GENERIC_READ
			, "UInt", 0x1				; FILE_SHARE_READ
			, Ptr, NULL
			, "UInt", 3				; OPEN_EXISTING
			, "UInt", 0x80				; FILE_ATTRIBUTE_NORMAL
			, Ptr, 0, Ptr)) = -1				; INVALID_HANDLE_VALUE
	{
msgbox, can't create file for`n%filename%
	return
	}
if !hFileMapping := DllCall("CreateFileMapping" AW
			, Ptr, hFile
			, Ptr, NULL
			, "UInt", 0x2				; PAGE_READONLY
			, "UInt", 0
			, "UInt", 0
			, Ptr, NULL, Ptr)
	{
	DllCall("CloseHandle", Ptr, hFile)
msgbox, can't create mapping for`n%filename%`nhFile=%hFile%
	return
	}
if !g_pMappedFileBase := DllCall("MapViewOfFile"
			, Ptr, hFileMapping
			, "UInt", 0x4				; FILE_MAP_READ
			, "UInt", 0
			, "UInt", 0
			, "UInt", 0, Ptr)
	{
	DllCall("CloseHandle", Ptr, hFileMapping)
	DllCall("CloseHandle", Ptr, hFile)
msgbox, can't create map view for`n%filename%`nhFile=%hFile% hMap=%hFileMapping%
	return
	}
MapStore(idx, 1, g_pMappedFileBase, hFileMapping, hFile)
return g_pMappedFileBase
}
;###############################################
UnmapFile(idx)
{
Global Ptr
MapStore(idx, 0, base, map, hfile)
DllCall("UnmapViewOfFile", Ptr, base)
DllCall("CloseHandle", Ptr, map)
DllCall("CloseHandle", Ptr, hfile)
}
;###############################################
MapStore(idx, op, ByRef base, ByRef map, ByRef hfile)
{
Static
ofi := A_FormatInteger
SetFormat, Integer, D
idx+=0
if op
	{
	b%idx% := base
	m%idx% := map
	h%idx% := hfile
	}
else
	{
	base := b%idx%
	map := m%idx%
	hfile := h%idx%
	}
SetFormat, Integer, %ofi%
}
;###############################################
;###############################################
GetPtrFromRVA(rva, nthdr, imageBase)
{
Global Ptr
if !pSectionHdr := GetEnclosingSectionHeader(rva, nthdr)
	return 0
delta := NumGet(pSectionHdr+0, 12, Ptr) - NumGet(pSectionHdr+0, 20, Ptr)	; VirtualAddress - PointerToRawData
;return (imageBase + rva - delta)
return (rva - delta)
}
;###############################################
GetPtrFromVA(vaptr, nthdr, imageBase)
{
rva := vaptr - NumGet(nthdr+0, 24+28, "UInt")	; OptionalHeader.ImageBase (this is ULONGLONG in x64)
return GetPtrFromRVA(rva, nthdr, imageBase)
}
;###############################################
GetEnclosingSectionHeader(rva, nthdr)
{
Global Ptr
return DllCall("dbghelp\ImageRvaToSection", Ptr, nthdr, Ptr, 0, Ptr, rva, Ptr)
Loop, % NumGet(nthdr+0, 6, "UShort")			; FileHeader.NumberOfSections
	{
	section := nthdr+(24+25*4+20*(A_PtrSize=8))+40*(A_Index-1)	; IMAGE_FILE_HEADER size + IMAGE_OPTIONAL_HEADER size + IMAGE_SECTION_HEADER*index
	if !size := NumGet(section+0, 8, "UInt")	; Misc.VirtualSize
		size := NumGet(section+0, 16, "UInt")	; SizeOfRawData
	va := NumGet(section+0, 12, "UInt")		; VirtualAddress
	if (rva >= va && rva < (va+size))
            return section
	}
return 0
}
