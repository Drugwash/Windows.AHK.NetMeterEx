; � Drugwash 2016-2019 v1.1

TrackMouseEvent(hwnd, t="L", time=0xFFFFFFFF)
{
Global
Static pf=0, pTME
if !pTME
	pTME := DllCall("GetProcAddress"
				, UPtr, DllCall("GetModuleHandle", "Str", "comctl32.dll", UPtr)
				, AStr, "_TrackMouseEvent"
				, UPtr)
f := t="H" ? 1 : t="L" ? 2 : t="N" ? 16 : t="C" ? (0x80000000 | pf) : 0x40000000
pf := (f & 0x13)
VarSetCapacity(TRACKMOUSEEVENT, sz := 12+PtrSz, 0)
NumPut(sz, TRACKMOUSEEVENT, 0, "UInt")					; cbSize
NumPut(f, TRACKMOUSEEVENT, 4, "UInt")					; dwFlags
NumPut(hwnd, TRACKMOUSEEVENT, 8, UPtr)				; hwndTrack
NumPut(time, TRACKMOUSEEVENT, 8+PtrSz, "UInt")			; dwHoverTime [ms]
return DllCall(pTME, Ptr, &TRACKMOUSEEVENT)				; comctl32.dll
}

/*
#include updates.ahk and call updates()
FLAGS:
TME_HOVER=0x1			--> WM_MOUSEHOVER
TME_LEAVE=0x2			--> WM_MOUSELEAVE
TME_NONCLIENT=0x10		--> WM_NCMOUSEHOVER, WM_NCMOUSELEAVE
TME_QUERY=0x40000000
TME_CANCEL=0x80000000	Cancel prior tracking request. Specify tracking type additionally to this flag

HOVER_DEFAULT=0xFFFFFFFF
*/
